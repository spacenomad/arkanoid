using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayState : BaseState
{
	[SerializeField]
	private GameObject paddleRef;
	private GameObject paddle;
	[SerializeField]
	private GameObject ballRef;
	private GameObject ball;
	[SerializeField]
	private GameObject brickContainerRef;
	private GameObject brickContainer;
	
	
	public override void Enter(Parameters playParameters)
	{
		Vector2 paddlePos = new Vector2(playParameters.paddlePosition[0], playParameters.paddlePosition[1]);
		Vector2 ballPos = new Vector2(playParameters.ballPosition[0], playParameters.ballPosition[1]);
		SpawnEntities(paddlePos, ballPos);

		Ball ballBehavior = ball.GetComponent<Ball>();
		ballBehavior.Wake();
		ballBehavior.Launch();

		Bricks bricksBehavior = brickContainer.GetComponent<Bricks>();
		bricksBehavior.Wake();
	}
	
	public override void Tick()
	{
		paddle.GetComponent<Paddle>().Tick();
		
		ball.GetComponent<Ball>().Tick();

		brickContainer.GetComponent<Bricks>().Tick();
	}

	public override void Exit()
	{
		Destroy(ball);
		Destroy(paddle);
		brickContainer.GetComponent<Bricks>().DestroyBricks();
		Destroy(brickContainer);
	}


	void SpawnEntities(Vector2 paddlePos, Vector2 ballPos)
	{
		paddle = Instantiate(paddleRef, GameManager.instance.entities.transform);
		paddle.transform.position = paddlePos;

		ball = Instantiate(ballRef, GameManager.instance.entities.transform);
		ball.transform.position = ballPos;

		brickContainer = Instantiate(brickContainerRef, GameManager.instance.entities.transform);
		brickContainer.transform.position = new Vector2(-1.5f, 0.8f);
	}

	
}
