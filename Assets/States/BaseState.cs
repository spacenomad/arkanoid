using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseState : MonoBehaviour
{
	public virtual void Enter(Parameters parameters) {}
	public virtual void Tick() {}
	public virtual void Exit() {}
}
