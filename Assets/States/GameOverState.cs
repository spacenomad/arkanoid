using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameOverState : BaseState
{
    public override void Enter(Parameters parameters)
    {
        GameManager.instance.displayText.GetComponent<TextMeshProUGUI>().text = "You lost. \n Press enter to try again";
    }

    public override void Tick()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            StateMachine.instance.Change(GameManager.instance.states[States.MENU], null);
        }
    }

    public override void Exit()
    {
        GameManager.instance.displayText.GetComponent<TextMeshProUGUI>().text = "";
    }
}
