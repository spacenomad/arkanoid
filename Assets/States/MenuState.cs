using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuState : BaseState
{
    public override void Enter(Parameters parameters)
    {
        GameManager.instance.displayText.GetComponent<TextMeshProUGUI>().text = "Welcome to Breakout! Press enter to start";
    }

    public override void Tick()
    {
    	if(Input.GetKeyDown(KeyCode.Return))
    		StateMachine.instance.Change(GameManager.instance.states[States.SERVE], null);
    }

    public override void Exit()
    {
        GameManager.instance.displayText.GetComponent<TextMeshProUGUI>().text = "";
    }
}
