using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServeState : BaseState
{
	[SerializeField]
	private GameObject paddleReference;
	private GameObject paddle;
	
	[SerializeField]
	private GameObject ballReference;
	private GameObject ball;
	
	
	public override void Enter(Parameters parameters)
	{
		SpawnEntities();
	}
	
	public override void Tick()
	{
		paddle.GetComponent<Paddle>().Tick();
		
		ball.transform.position = new Vector3(paddle.transform.position.x, paddle.transform.position.y + 0.15f, paddle.transform.position.z);
		
		
		
		if(Input.GetKeyDown(KeyCode.Return))
		{
			float[] paddlePos = {paddle.transform.position.x, paddle.transform.position.y};
			float[] ballPos = {ball.transform.position.x, ball.transform.position.y};
			Parameters playParameters = new Parameters(paddlePos, ballPos, 0);
			StateMachine.instance.Change(GameManager.instance.states[States.PLAY], playParameters);
		}
	}
	
	public override void Exit()
	{
		DestroyEntities();
	}
	
	
	
	void SpawnEntities()
	{
		paddle = Instantiate(paddleReference, GameManager.instance.entities.transform);
		paddle.transform.position = new Vector2(paddle.transform.position.x, -0.8f);
		
		ball = Instantiate(ballReference, GameManager.instance.entities.transform);
		ball.transform.position = new Vector2(ball.transform.position.x, -0.6f);
	}
	
	void DestroyEntities()
	{
		Destroy(paddle);
		Destroy(ball);
	}
}
