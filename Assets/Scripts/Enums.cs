using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum States 
{
	MENU,
	SERVE,
	PLAY,
	GAME_OVER
}

public enum Layers
{
	PADDLE = 9,
	WALLS = 10,
	BRICKS = 11,
	LOSE = 12
}