using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parameters
{
    [SerializeField]
    public float[] paddlePosition;
    [SerializeField]
    public float[] ballPosition;
    [SerializeField]
    public float score;

    public Parameters(float[] paddlePos, float[] ballPos, float scr)
    {
        paddlePosition = paddlePos;
        ballPosition = ballPos;
        score = scr;
    }
}
