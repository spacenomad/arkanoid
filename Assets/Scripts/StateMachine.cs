using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
	public static StateMachine instance {get; private set; }
	
	
	public BaseState current;
	private GameObject stateObject;
	
	
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
	
	public void Change(BaseState newState, Parameters parameters)
	{
		if (current != null)
			current.Exit();
		current = newState;
		Destroy(stateObject);
		stateObject = Instantiate(current.gameObject, this.transform);
		
		
		current.Enter(parameters);
	}
	
	public void Tick()
	{
		if (current != null)
			current.Tick();
	}
}
