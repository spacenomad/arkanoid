using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public static GameManager instance {get; private set; }
	
	
	[Space]
	[Header ("Main Actors")]
	[SerializeField]
	public GameObject managers;
	[SerializeField]
	public GameObject entities;
	[SerializeField]
	public GameObject canvas;
	[SerializeField]
	public GameObject displayText;
	
	//https://freesound.org/people/sonically_sound/sounds/624874/
	
	[Space]
	[Header ("Initialize for Dictionaries")]
	
	[SerializeField]
	BaseState[] statesToInitialize = new BaseState[4];
	
	//[SerializeField]
	//AudioSource[] soundsToInitialize = new AudioSource[3];
	
	
	
	public Dictionary<States, BaseState> states = new Dictionary<States, BaseState>();
	
	//public Dictionary<string, AudioSource> sounds = new Dictionary<string, AudioSource>();
	
	
	
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
	
	void Start()
	{
		
		states.Add(States.MENU, statesToInitialize[0]);
		states.Add(States.SERVE, statesToInitialize[1]);
		states.Add(States.PLAY, statesToInitialize[2]);
		states.Add(States.GAME_OVER, statesToInitialize[3]);
		
		
		//sounds.Add("jump", soundsToInitialize[0]);
		//sounds.Add("score", soundsToInitialize[1]);
		//sounds.Add("death", soundsToInitialize[2]);
		
		
	
		StateMachine.instance.Change(states[States.MENU], null);
	}

	void Update()
	{
		StateMachine.instance.Tick();
	}
	
	
	
}
