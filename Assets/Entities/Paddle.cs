using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
	[SerializeField]
	public float SPEED;
	
	[System.NonSerialized]
	public float direction;


	public void Tick()
	{
		this.direction = Input.GetAxisRaw("Horizontal");
		this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x + this.direction * this.SPEED * Time.deltaTime, -1.45f, 1.45f), this.transform.position.y, this.transform.position.z);
	}
}
