using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bricks : MonoBehaviour
{
    [SerializeField]
    public GameObject brickRef;
    public GameObject[,] bricks = new GameObject[9,6];

    [SerializeField]
    public Sprite[] brickSkins;

    public void Wake()
    {
        SpawnBricks();
    }

    void SpawnBricks()
    {
        float xInterval = 0.32f;
        float yInterval = -0.16f;
        
        for(int y = 0; y < 6; y++)
        {
            for(int x = 0; x < 9; x++)
            {
                bricks[x, y] = Instantiate(brickRef, this.gameObject.transform);
                bricks[x, y].transform.position = new Vector2(x * xInterval - 1.3f, y * yInterval + 0.8f);
                bricks[x, y].GetComponent<SpriteRenderer>().sprite = brickSkins[y * 3];
            }
        }
    }

    public void Tick()
    {
        for(int y = 0; y < 6; y++)
        {
            for(int x = 0; x < 9; x++)
            {
                if(bricks[x, y] == null)
                {
                    continue;
                }

                bricks[x, y].GetComponent<Brick>().Tick();
            }
        }
    }

    public void DestroyBricks()
    {
        for(int y = 0; y < 6; y++)
        {
            for(int x = 0; x < 9; x++)
            {
                Destroy(bricks[x, y]);
            }
        }
    }
}
