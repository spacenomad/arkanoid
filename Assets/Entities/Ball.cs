using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	[SerializeField]
	public float SPEED;

	[System.NonSerialized]
	public Rigidbody2D rigidbody;
	
	[System.NonSerialized]
	public Vector2 direction;

	public void Wake()
	{
		rigidbody = this.GetComponent<Rigidbody2D>();
	}
	
	public void Launch()
	{
		float x = Random.Range(0, 2) == 0 ? -1 : 1;
		float y = 1;
		direction = new Vector2(x, y);
	}

	public void Tick()
	{
		this.rigidbody.velocity = this.direction * this.SPEED;
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		GameObject target = collider.gameObject;
		BoxCollider2D boxCollider = target.GetComponent<BoxCollider2D>();

		if(target.layer == (int)Layers.WALLS)
		{
			if(this.transform.position.x > target.transform.position.x + boxCollider.size.x / 2)
			{
				this.direction = new Vector2(-this.direction.x, this.direction.y);
				this.transform.position = new Vector2(this.transform.position.x + this.GetComponent<BoxCollider2D>().size.x, this.transform.position.y);
			}

			if(target.transform.position.x - boxCollider.size.x / 2 > this.transform.position.x)
			{
				this.direction = new Vector2(-this.direction.x, this.direction.y);
				this.transform.position = new Vector2(this.transform.position.x - this.GetComponent<BoxCollider2D>().size.x, this.transform.position.y);
			}

			if(target.transform.position.y - boxCollider.size.y / 2 > this.transform.position.y)
			{
				
				this.direction = new Vector2(this.direction.x, -this.direction.y);
				this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y - this.GetComponent<BoxCollider2D>().size.y);
			}
		}

		if(target.layer == (int)Layers.BRICKS)
		{
			target.GetComponent<Brick>().remove = true;

			if(this.transform.position.x > target.transform.position.x + boxCollider.size.x / 2)
			{
				this.direction = new Vector2(-this.direction.x, this.direction.y);
				this.transform.position = new Vector2(this.transform.position.x + this.GetComponent<BoxCollider2D>().size.x, this.transform.position.y);
			}
			else if(target.transform.position.x - boxCollider.size.x / 2 > this.transform.position.x)
			{
				this.direction = new Vector2(-this.direction.x, this.direction.y);
				this.transform.position = new Vector2(this.transform.position.x - this.GetComponent<BoxCollider2D>().size.x, this.transform.position.y);
			}
			else if(target.transform.position.y - boxCollider.size.y / 2 > this.transform.position.y)
			{
				
				this.direction = new Vector2(this.direction.x, -this.direction.y);
				this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y - this.GetComponent<BoxCollider2D>().size.y);
			}
			else 
			{
				this.direction = new Vector2(this.direction.x, -this.direction.y);
				this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + this.GetComponent<BoxCollider2D>().size.y);
			}
		}

		if(target.layer == (int)Layers.PADDLE)
		{
			if(target.transform.position.x > this.transform.position.x)
			{
				this.direction = new Vector2(2 * (-Mathf.Abs(this.transform.position.x - target.transform.position.x)), this.direction.y);
			}
			else
			{
				this.direction = new Vector2(2 * (Mathf.Abs(this.transform.position.x - target.transform.position.x)), this.direction.y);
			}

			this.direction = new Vector2(this.direction.x, -this.direction.y);
			this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + this.GetComponent<BoxCollider2D>().size.y + 0.01f);
		}

		if(target.layer == (int)Layers.LOSE)
		{
			StateMachine.instance.Change(GameManager.instance.states[States.GAME_OVER], null);
		}
	}
}
